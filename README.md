# README #

## RESSOURCES ##

## Version 2.0.0.6 ##

### What is this repository for? ###

* This repo contains all static files, which are needed by marketing-in-one generated pages and webhooks.

### How do I get set up? ###

* Summary of set up: Check if correct file (rw-r--r--) / folder permissions (rwxrx-r--)are set on ressources.marketing-in-one.net
* Configuration: ./.
* Dependencies: marketing-in-one.net, top-aktionen.net/com, webhook-in-one.net, generator.top-aktionen.net/com, generator.webhook-in-one.net, 
* Database configuration: ./.
* How to run tests: gernated page or webhook in MIO and open draft or active version.
* Deployment instructions: login into PLESK marketing-in-one.net, choose subdomain ressources.marketing-in-one.net and run the git installer. Done.

### Contribution guidelines ###

* Writing tests: ./.
* Code review: ./.
* Other guidelines: ./.

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
