/**
 * @internal
 * Main configuration for client page
 *Input:takes Settings and token value from files
 *Functions:
 *      1.Generate Form todo:remove it use templates from backend
 *      2.Send the lead data to AIO
 *      3.Send data to matomo
 *      4.Cookie information processing and saving
 *      5.todo:Splittest
 *      6.todo:page flow
 *      7.todo:client tracking b.z url,IP etc.. tracking information feature move to backend
 */


//@internal-get Token Value.
let token_value_txt = function () {
    let jsonTemp = null;
    $.ajax({
        'async': false,
        'url': 'token.txt',
        'success': function (data) {
            jsonTemp = data;
        }
    });
    return jsonTemp;
}();

//@internal-taking settings from the settings file
let settingsValue = function () {
    let jsonTemp = null;
    $.ajax({
        'async': false,
        'url': 'settings.txt',
        'success': function (data) {
            jsonTemp = data;
        }
    });
    return jsonTemp;
}();

//@internal- global variables
let basicConfig;
let apiInOneSystem;
let basePayLoad = [];
let mappingTable;
let HtmlFormValues;
let activeProcesshook = false;
let processhookStatus;
//@internal- divs as consts
const formPage = $('#form');
const successPage = $('#success');

//@internal-extract settings for multiple use cases
if (settingsValue) {
    basicConfig = JSON.parse(atob(settingsValue));
    apiInOneSystem = basicConfig['additionalSettings']['AIOUrl'];

    //@internal- Variables for processhooks
    let processhookStatus;

    if (basicConfig) {
        $.each(basicConfig, function (key, value) {
            switch (key) {
                case 'mapping':
                    mappingTable = JSON.parse(atob(value['mappingTable']))['contact'];
                    let formValues = [];
                    $.each(mappingTable, function (index, element) {
                        $.each(element, function (key, value) {
                            let HtmlForm = Object.keys(value)[0];//take first element key
                            let isRequired = value['required'];
                            if (HtmlForm !== "") {
                                let generateHtml = {[HtmlForm]: isRequired};
                                formValues.push(generateHtml);
                            }
                        });
                    });
                    HtmlFormValues = formValues;
                    break;
                case 'tracker':
                    //Map the tracking settings
                    //this is moved to tracking.js
                    break;
                case 'additionalSettings':
                    // Map additional settings required to create Contact.
                    basePayLoad['apikey'] = value['apiKey'];
                    basePayLoad['objectregister_id'] = value['objectregister_id'];
                    basePayLoad['accountNumber'] = value['accountNumber'];
                    break;
                case 'processhooks':
                    //make buttons for each processhook and create events
                    activeProcesshook = true;
                    break;
                case 'processhookStatus':
                    processhookStatus = value;
                    break;
                default:
                // code block
            }
        });
    }
}

/*
    @internal - on page load functions
    @internal each code snippet has some functionality for the client
 */
$(document).ready(function () {

    //@internal - tracking client function found in tracking js
    if($.cookie ( 'cookieConsent' )){
        manageTracking();
    }

    //@internal- Generate html and append it
    if (HtmlFormValues) {
        generateForm(HtmlFormValues);
    }

    //@internal- append policy urls
    if (basePayLoad['accountNumber']) {
        appendPolicyUrls(basePayLoad);
    }

    //@internal- generate buttons
    if (activeProcesshook) {
        appendButtons(processhookStatus);
    }

    //@internal- send data to AIO
    $('.formButton').click(function (e) {
        //@internal - global const
        const form = $('.check-submit-form');
        e.preventDefault();
        this.disabled = true;
        let formData = form.serializeArray();
        //@internal- management of data and flow
        sendDataToAIO(formData);
        manageFlow();
    });

});


/**
 *
 * @return Html text.
 * @param key
 * @param value
 * @internal This function is used to generate Html for form
 */
function generateHtml(key, value) {
    //todo: place this in the html and use string replace or jquery
    let UrlParams = ["permission", "referrer_id", "affiliate_id", "affiliate_sub_id"];
    let returnHtml;
    let requiredField = '';
    if (value === 'true') {
        requiredField = '*';
    }
    let placeHolderKey = key.replace('Contact_', '');
    if (key === 'salutation') {
        returnHtml = `
                    <div class="form-row">
                       <div class="form-group col-md-12">
                           <div style="width:100%;" class="btn-group btn-group-toggle" data-toggle="buttons">
                             <label style="cursor:pointer; width:100%; font-family: 'Titillium Web', sans-serif !important; font-weight: 300 !important; font-size: 1.1rem !important;" class="btn btn-outline-light">
                                <input class="sr-only" style="color:#fff; ;background:transparent;" value="Frau" type="radio" name="` + key + `" id="option1" autocomplete="off" required> Frau
                                     </label>
                                     <label style="cursor:pointer; width:100%; font-family: 'Titillium Web', sans-serif !important; font-weight: 300 !important; font-size: 1.1rem !important;" class="btn btn-outline-light">
                                <input class="sr-only" style=" color:#fff; ;background:transparent;" value="Herr" type="radio" name="` + key + `" id="option2" autocomplete="off" required> Herr
                             </label>
                           </div>
                        </div>
                     </div>
                    `;
    } else if (key === 'email') {
        returnHtml = `
                     <div class="form-row">
                       <div class="form-group col-md-12">
                       <input style="color:#000; background:#FFF;" value="" class="btn-outline-light input-fields form-control" id="` + key + `" name="` + key + `" placeholder="` + placeHolderKey + `` +
            requiredField + `" type="email">
                        </div>
                    </div>
                    `;
    } else if (UrlParams.includes(key)) {
        returnHtml = `
                     <div class="form-row" hidden>
                       <div class="form-group col-md-12">
                       <input style="color:#000; background:#FFF;" value="" class="btn-outline-light input-fields form-control" id="` + key + `" name="` + key + `" placeholder="` + placeHolderKey + `` + requiredField + `" type="text"
                        </div>
                    </div>
                    `;
    } else {
        returnHtml = `
                     <div class="form-row">
                       <div class="form-group col-md-12">
                       <input style="color:#000; background:#FFF;" value="" class="btn-outline-light input-fields form-control" id="` + key + `" name="` + key + `" placeholder="` + placeHolderKey + `` + requiredField + `" type="text" required
                        </div>
                    </div>
                    `;
    }
    return returnHtml;
}


/**
 * @param {array} processhookStatus
 * @author AKi
 * @comment generates additional buttons with different works flows
 */
function appendButtons(processhookStatus) {
    const buttonsHtml = $('#buttons');
    $.each(processhookStatus, function (key, val) {
        if (key === "index.html") {
            $.each(val, function (index, value) {
                if (index !== "Submit") {
                    let button = '<button class="btn treaction-btn-form customButton formButton" id="' + index + '" ' +

                        'objectRegisterId="' + value['objectRegisterId'] + '" ' +
                        'workflowId="' + value['workflowId'] + '"  style="margin-top: 5px;' +
                        ' width: 100%">' + index + ' &nbsp;</button>';
                    buttonsHtml.append(button);
                } else {
                    $('#Submit').attr('workflowId', value['workflowId']);
                }
            });
        }
    });
}

/**
 *
 * @return json.
 * @param basePayLoad
 * @param formData
 * @param mappingTable
 * @internal this function generate structure required for AIO
 */
function structureFormData(basePayLoad, formData, mappingTable) {
    $.each(mappingTable, function (index, element) {
        $.each(element, function (key, value) {
            let name = Object.keys(value)[0];
            let data = "";
            $.each(formData, function (a, b) {
                if (b.name === name) {
                    data = b.value;
                }
            });
            value[name] = data;
        });
    });
    let dataFormat = {
        "base": {
            "apikey": basePayLoad['apikey'],
            "account_number": basePayLoad['accountNumber'],
            "object_register_id": basePayLoad['objectregister_id'],
            //"campaign_token": token_value[0]['value']
        },
        "contact": mappingTable
    };
    return dataFormat;
}

/**
 * Unicode to ASCII (encode data to Base64)
 * @param {string} data
 * @return {string}
 */
function utoa(data) {
    return btoa(unescape(encodeURIComponent(data)));
}

/**
 * @internal get policy data for page and appends to the url of the respected variables
 * @param {array} baseData
 * @return {void}
 */
function appendPolicyUrls(baseData) {
    let data;
    //format the data in required form
    data = {
        "apikey": baseData['apikey'],
        "account_number": baseData['accountNumber'],
        "object_register_id": baseData['objectregister_id']
    };
    //encrypt the data and send to AIO
    let encryptedData = utoa(JSON.stringify(Object.assign({}, data)));
    let getPolicyDataFromAio = {
        'async': true,
        "url": apiInOneSystem + "/page/policy/get",
        "method": "POST",
        "timeout": 0,
        "headers": {
            "Content-Type": "text/plain"
        },
        "data": encryptedData,
    };
    //set the parameters in html
    $.ajax(getPolicyDataFromAio).done(function (response) {
        if (response['url_privacy_policy']) {
            $('#privacy_policy').attr('href', response['url_privacy_policy']);
            $('#cookiePrivacyPolicy').attr('href', response['url_privacy_policy']);
        }
        if (response['url_imprint']) {
            $('#imprint').attr('href', response['url_imprint']);
        }
        if (response['url_gtc']) {
            const agbTag = $('#agb');
            agbTag.attr('href', response['url_gtc']);
            agbTag.removeAttr('hidden');
        }
        if (response['website']) {
            const websiteTag = $('#website');
            websiteTag.attr('href', response['website']);
            websiteTag.text(response['website']);
        }
    });
}

/**
 * @internal- checks if url params exists and returns url params if exists.
 * @returns {bool}
 */
function CheckUrlParams(){
    let url = window.location.href;
    if(url.indexOf('?') != -1)
        return true;
    else if(url.indexOf('&') != -1)
        return true;
    return false;
}

/**
 * @internal -checks if url params exists and returns url params if exists.
 * @returns {string}
 */
function generateUrlWithParams(url){
    let currentUrl = window.location.href;
    let parts = currentUrl.split('?', 2);
    let params = parts[1];
    let redirectUrl = url + '?' + params;
    return redirectUrl;
}

/**
 * @internal -Generates form.
 * @returns {void}
 */
function generateForm(formValues){
    let html;
    $.each(formValues, function (index, val) {
        $.each(val, function (key, value) {
            html = generateHtml(key, value);
            $('#marketingInOneForm').append(html);
        });
    });
}

/**
 * @internal -Sends data to AIO.
 * @returns {boolean}
 */
function sendDataToAIO(data){
    //structured form data with token
    let formDataWithToken = structureFormData(basePayLoad, data, mappingTable);
    //if processhook is active
    if (activeProcesshook) {
        formDataWithToken['base']['workflow_id'] = $(this).attr('workflowId');
    }
    let formJson = JSON.stringify(formDataWithToken);
    let formDataSent = utoa(formJson);
    let AioResponse = {
        "url": apiInOneSystem + "/v2.0/contact/create",
        "method": "POST",
        'async': true,
        'success': 'success',
        "timeout": 0,
        "headers": {
            "Content-Type": "text/plain"
        },
        "data": formDataSent
    };
    $.ajax(AioResponse).done(function (response) {
        return true;
    });
    return false;
}

/**
 * @internal -Sends data to AIO.
 * @returns {void}
 */
function manageFlow(){
    //internal- first hide the div - this solves the empty white page problem
    successPage.hide();
    successPage.load("success.html");
    setTimeout(function () {
        //@internal - this are body handling functions todo:make this a separate function with flow control
        formPage.html('');
        successPage.fadeIn('10');
        //@internal-redirect to client page
        setTimeout(function () {
            let redirectUrl = basicConfig['page_flow']['redirect_page'];
            //@internal - forward url params
            if (CheckUrlParams()) {
                redirectUrl = generateUrlWithParams(redirectUrl);
            }
            $(location).attr('href', redirectUrl);
        }, 5000);
    }, 500);
}


